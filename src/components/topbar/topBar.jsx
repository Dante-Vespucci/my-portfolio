import "./topBar.scss"
import {Face, Mail} from "@material-ui/icons"

export default function Topbar({menuOpen, setMenuOpen}) {
  return (
    <div className={"topBar " + (menuOpen && "active")}>
        <div className="wrapper">
          <div className="left">
            <a href="#intro" className="logo">
              genius.io
            </a>
            <div className="itemContainer">
              <Face className="icon"/>
              <span>+1 Not-My-Number</span>
            </div>
            <div className="itemContainer">
              <Mail className="icon"/>
              <span>HotBox@EmuLovers.com</span>
            </div>
          </div>
          <div className="right">
          <div className="hamburger" onClick={()=>setMenuOpen(!menuOpen)}>
            <span className="line1"></span>
            <span className="line2"></span>
            <span className="line3"></span>
          </div>
          </div>
        </div>
    </div>
  )
}
