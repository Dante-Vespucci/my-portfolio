import { useState } from "react";
import "./contact.scss";

export default function Contact() {
  const [message, setMessage] = useState(false);
  const [sentData, setSentData] = useState("email");
  const [sentEmail, setSentEmail] = useState("message");


  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      //console.log("ACTUAL backend url: " + process.env.REACT_APP_BACKEND_URL); Fixed. Wrong location .env
      let res = await fetch(process.env.REACT_APP_BACKEND_URL, {
        method: "POST",
        headers: {'Content-Type': 'application/json' },
        body: JSON.stringify({
          email_address: sentEmail,
          message: sentData,
        }),
      }).then((data) => {
        //console.log("DATA BODY: " + data);
        return data;
      });
      //console.log("RES BODY: " +  res.body + " RES STATUS: " + res.status);
      let resJson = await res;
      if (resJson.status === 200) {
        setMessage(true);
      } else {
        setMessage(false);
      }
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <div className="contact" id="contact">
      <div className="left">
        <img src="assets/shake.svg" alt="" />
      </div>
      <div className="right">
        <h2>Contact.</h2>
        <form onSubmit={handleSubmit}>
          <input type="text" placeholder="Email" onChange={e => {
            setSentEmail(e.target.value);
          }} />
          <textarea placeholder="Message" onChange={d => {
            setSentData(d.target.value);
          }}></textarea>
          <button type="submit">Send</button>
          {message && <span>Thanks, I'll reply ASAP :)</span>}
        </form>
      </div>
    </div>
  );
}