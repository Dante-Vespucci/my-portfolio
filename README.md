# Pipeline Fullstack Project Using React, Express, and DynamoDB
![Frontend Pipeline Status](https://gitlab.com/Dante-Vespucci/my-portfolio/badges/main/pipeline.svg)
![Backend Pipeline Status](https://gitlab.com/Dante-Vespucci/my-portfolio-express-backend/badges/main/pipeline.svg)

[[_TOC_]]

## Description
This project was originally conceived in order to improve my understanding of React and showcase my pipeline fundamentals through. 

### The Evolution
My Project somewhat evolved after finishing my front end to include an Express backend to talk to DynamoDB through, when I realized I wanted to achieve a fullstack project, rather than just frontend.

### Final Project
The frontend is a simple portfolio template made with a combination of JSX and SCSS files. It features 5 seperate pages with quite a few scss formatting tricks throught it, as well as a flex display with the added ability to adapt to mobile devices. The backend, which you can find the source code to in the second pipeline status badge, connects my React code with DynamoDB in order to store the information sent to me through the contact page of my portfolio.

An overall flow of the project can be found here: 
![Drawio File](./assets/flow.drawio.svg)

## Technology Stack
The two languages used for this project are javascript and css. My frontend was deployed to a site called netlify. This is a site for individual developers or small businesses to work on creating sites for free and hosting them remotely, with a limited amount of build time per month being your only limiting factor. The backend is hosted on Heroku. Both deployments are done within their respective pipelines automatically. Due to the fact there is a build limit on netlify, I have made the deploy step for the front-end manual, so no limits were reached.

## Accessing the Site
The website can be found at:

```
https://serene-choux-f3d7f1.netlify.app
```

This URL may change everytime the frontend is deployed to netlify, so the deploy step holds the definitive state of the url.

## Issues
There were a few bumps in the road during the creation of this project. This was my first time developing a website of any kind, so it came with it's challenges.

### Netlify's Strange Access
If you look at my yml file in this project for deploying to Netlify, you'll see a strange indicator that I had to attatch to the install.

```
npm install -g --unsafe-perm=true netlify-cli
```

By setting unsafe-perm to true, npm is forced to always run within the context of the running script. This, in and of itself, is not unsafe, but it could allow the script to install packages from root. That is why it is called unsafe-perm. As for why netlify cli needs this, the only thing I've found is a response in their forums stating: "It is due to some changes we've introduced in the last version. We'll be updating this issue with any updates on this topic. Until then, if you must install the CLI under the root user, we recommend that you use the flag." Some questions are better left vaguely answered I guess.

### The Frontend Backend Distinction
Originally, I wanted to make this project simple and only have one project for both frontend and backend. It turns out, when react-scripts were bumpted to version 5, they wanted to stiffle people who put front and backend together. 

An error similar to this started to occur after I installed dotenv and aws-sdk with npm.

```
ERROR in ../../node_modules/dotenv/lib/main.js 26:13-28
Module not found: Error: Can't resolve 'path' in '../node_modules/dotenv/lib'

BREAKING CHANGE: webpack < 5 used to include polyfills for node.js core modules by default.
This is no longer the case. Verify if you need this module and configure a polyfill for it.

If you want to include a polyfill, you need to:
	- add a fallback 'resolve.fallback: { "path": require.resolve("path-browserify") }'
	- install 'path-browserify'
If you don't want to include a polyfill, you can use an empty module like this:
	resolve.fallback: { "path": false }
```

This occured because, as it says in the error, "webpack < 5 used to include polyfills for node.js core modules by default.
This is no longer the case." There were a few solutions around this that included modifying the node_modules that were imported by npm and messing with code that was far too deep into the weeds for me. So, this is what prompted the evolution of my project from one for both front and backend to two. 

## TODO
 - Add unit testing through Jest within the code (pipeline parts already set)
 - Add more reactiveness to different screen sizes
 - Add an error catch for invalid contact information.
 - Add graceful error catches for invalid html links used as images.
 - Add another component page to recieve feedback and query from that dynamo db table to populate said page with recent feedback.
	- Requires more work on backend side for querying as well.

## Creds and Acknowledgements

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

The frontend for this application was done through a tutorial at https://www.youtube.com/watch?v=7WwtzsSHdpI
The source code can be found here: https://github.com/safak/youtube/tree/react-portfolio

Biggest credit goes to my instructor Dennis Huynh for his contributions.
